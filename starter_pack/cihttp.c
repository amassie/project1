#include "cihttp.h"

void *GetInAddr(struct sockaddr *sa)
{
    printf("GetInAddr");
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

void AddHeaderToRequest(struct httprequest *req, char *name, char *value)
{
    NVL *header = malloc(sizeof(NVL));
    header->name = name;
    header->value = value;
    if (req->headers)
        header->next = req->headers;
    else
        header->next = NULL;
    req->headers = header;
}

void AddHeaderToResponse(struct httpresponse *res, char *name, char *value)
{
    NVL *header = malloc(sizeof(NVL));
    header->name = name;
    header->value = value;
    if (res->headers)
        header->next = res->headers;
    else
        header->next = NULL;
    res->headers = header;
    printf("New Header :%s\n", res->headers);
}



struct httprequest *ParseHTTPRrequest(char *data)
{
    printf("\n%s",data);
    struct httprequest *req;
    req = malloc(sizeof(struct httprequest));;

    char* token;
    token = strtok(data, " ");
    printf("\n\nMETHOD: %s\n",token);
    int counter=0;
    NVL* headerTemp;

    if(( token) != NULL )
    {
        if(strcmp(token,"GET")==0)
        {
            printf("GET\n");
            req->method=GET;
        }
        else if(strcmp(token,"HEAD")==0)
        {
            printf("HEAD\n");
            req->method=HEAD;
        }
        else if(strcmp(token,"POST")==0)
        {
            printf("POST\n");
            req->method=POST;
        }
        else{
            printf("\nMETHOD FOUND NO MATCH\n");
        }
    }
    if(( token = strtok(NULL, " ")) != NULL )
    {
        printf("URI: %s\n", token);
        req->request_uri =malloc(sizeof(token)+1);
        strcpy(req->request_uri, token);
    }
    if(( token = strtok(NULL, " ")) != NULL )
    {
        printf(" %s\n", token);
        req->version = (float) *token;
    }

    //printf("ADDING HEADERS\n");
    req->headers=malloc(sizeof(NVL));
    headerTemp=req->headers;

    char *nameToken;
    while((nameToken = (char*)strtok(NULL, ":"))!=NULL && (token = (char*)strtok(NULL, "\r\n"))!=NULL)
    {
        headerTemp->name = malloc(sizeof(char) * strlen(nameToken)+1);
        strcpy(headerTemp->name, nameToken);
        //printf("%s*  :",headerTemp->name);

        headerTemp->value = malloc(sizeof(char) * strlen(token)+1);
        strcpy(headerTemp->value, token);
        //printf("%s*",headerTemp->value);

        headerTemp->next=malloc(sizeof(NVL));//this will malloc 1 too many but I'm too lazy to fix it
        headerTemp = headerTemp->next;

    }

    if(req->method==POST)
    {
        if (nameToken!=NULL)
        {
            printf("\nFOUND THE POST\n");
            token=strtok(nameToken,"\r\n");
            while(token=="\r\n")
            {
                //printf("extra RN\n");
                token=strtok(nameToken,"\r\n");
            }
            //printf("cleaned token %s\n",token);
            req->body = malloc(sizeof(char) * strlen(token));
            strcpy(req->body, token);
            printf("POST: %s", req->body);
        }
    }

    return req;
}

void PrintHTTPRequest(struct httprequest *req)
{
    printf("\nPRINT\n");

    printf(" HTTP request:\n %d\n %s\n %f\n",req->method,req->request_uri,req->version);
    NVL* headerTemp=req->headers;
    printf("Headers:\n");
    while(headerTemp!=NULL &&headerTemp->name!=NULL &&headerTemp->value!=NULL)
    {
        printf("%s: %s\n",headerTemp->name, headerTemp->value);
        headerTemp=headerTemp->next;
    }
    if(req->method==POST)
    {
        printf("%s\n",req->body);
    }
}

struct httpresponse *GenerateHTTPResponse(struct httprequest *req)
{



    printf("\nGenerateHTTPResponse\n");
    struct httpresponse *res = malloc(sizeof(struct httpresponse));
    char url[MAX_LEN]="www";
    strcat(url, req->request_uri);
    printf("URL: %s\n",url);

    struct stat attr;
    stat(url, &attr);
    char last_modified[64];
    struct tm* time = gmtime(&attr.st_mtime);
    strftime(last_modified, 64, "%a, %d %b %G %T GMT", time);
    printf("Last modified time: %s\n", last_modified);
    AddHeaderToRequest(req,"Last-Modified", last_modified );
    if((req->method==HEAD ||req->method==GET) && attr.st_size>0)
    {
        res->status = 200;
        res->reason = malloc(sizeof("OK"));
        res->reason="OK";
        res->version = 1.1f;

        if(req->method==GET)
        {
            printf("FILE\n");
            FILE* file=fopen(url,"r");
            if(file!=NULL){
                res->body=malloc(sizeof(char)*attr.st_size+1);
                printf("\n");
                char currentCharacter=' ';
                fread(res->body, sizeof(char),attr.st_size ,file);

                printf("%s\n", res->body);
           }
            else{
                printf("NULL file\n");
            }
        }
        else{
            printf("no file to get\n");
        }
    }
    else if(req->method==POST)
    {
        res->status = 200;
        res->reason = malloc(sizeof("OK")+1);
        res->reason="OK";
        res->version = 1.1f;

        char temp[MAX_LEN]="";
        char body[MAX_LEN]="";

        int counter=0;
        int otherCounter=0;
        for(counter=0;counter<strlen(req->body);counter++,otherCounter++)
        {
            if(req->body[counter]=='&'){
                body[otherCounter]='<';
                otherCounter++;
                body[otherCounter]='b';
                otherCounter++;
                body[otherCounter]='r';
                otherCounter++;
                body[otherCounter]='>';
            }
            else if(req->body[counter]=='+'){
                body[otherCounter]=' ';
            }
            else if(req->body[counter]=='='){
                body[otherCounter]=':';
            }
            else{
                body[otherCounter]=req->body[counter];
            }
        }

        res->body=malloc(sizeof(char)*(strlen(body)+1) );
        sprintf(temp,"<html><body><h1 style='border: 1px solid; text-align:center'>%s<h1></body></html>",body);
        strcpy(res->body,temp);
    }
    else{
        printf("couldn't find file!\n");
        res->status = 404;
        res->reason = malloc(sizeof("File not found")+1);
        res->reason="File not found";
        res->version = 1.1f;
    }

    if(req->headers!=NULL){
        res->headers=req->headers;
    }
    return res;
}

void SendHTTPResponse(struct httpresponse *res, int cfd_socket)
{
    // TODO: incomplete
    printf("sendHTTPResponse\n");

    char temp[MAX_LEN]="";
    char data[MAX_LEN]="";
    printf("HTTP/%.2f %d %s\n",res->version, res->status, res->reason);
    sprintf(temp,"HTTP/%.2f %d %s\r\n ",res->version, res->status, res->reason);//"HTTP/1.1 400 OK.\r\nserver:cihttp\r\n\r\n<html><body>maybe?</body></html>";
    strcat(data,temp);
    temp[0]='\0';

    sprintf(temp,"Date: Wed Sep 19 15:20:54 2018\r\n server: cihttp\r\n\r\n");
    strcat(data,temp);
    temp[0]='\0';

    if(res->body!=NULL){
        sprintf(temp,"%s\r\n",res->body);
        strcat(data,temp);
    }
*/

    strcat(data,"\r\n\r\n");

     send(cfd_socket, data, strlen(data), 0);

}

int main(int argc, char *argv[])
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    struct sockaddr_storage their_addr;
    int sfd, optval, res, cfd_socket;
    socklen_t sin_size;
    char ip[INET_ADDRSTRLEN];

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;


    res = getaddrinfo(NULL, PORT, &hints, &result);
    if (res != 0)
    {
        fprintf(stderr, "getaddrinfo() error: %s\n", strerror(errno));
    }


    for (rp = result; rp != NULL; rp = rp->ai_next)
    {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1)
        {
            fprintf(stderr, "socket() error: %s\n", strerror(errno));
            continue;
        }

        optval = 1;
        res = setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
        if (res != 0)
        {
            close(sfd);
            fprintf(stderr, "setsockopt() error: %s\n", strerror(errno));
            exit(1);
        }

        res = bind(sfd, rp->ai_addr, rp->ai_addrlen);
        if (res != 0)
        {
            close(sfd);
            fprintf(stderr, "bind() error: %s\n", strerror(errno));
            continue;
        }

        break;
    }

    freeaddrinfo(result);

    if (rp == NULL)
    {
        fprintf(stderr, "server: failed to bind\n");
        exit(1);
    }

    res = listen(sfd, BACKLOG);
    if (res != 0)
    {
        fprintf(stderr, "listen() error: %s\n", strerror(errno));
        exit(1);
    }



    for (;;)// whyyyyy? it could just be a while loop. This pains me...
    {
        printf("\nfor Enter\n");

        sin_size = sizeof their_addr;

        cfd_socket = accept(sfd, (struct sockaddr *)&their_addr, &sin_size);
        if (cfd_socket == -1)
        {
            fprintf(stderr, "accept() error: %s\n", strerror(errno));
            continue;
        }
        switch (fork())
        {
            case -1: // error
                fprintf(stderr, "fork() can't create child: %s\n", strerror(errno));
                close(cfd_socket);
                break;
            case 0: // child
            {
                char *data = malloc(MAX_LEN * sizeof(char));
                int res;

                // child doesn't need listening socket
                close(sfd);

                inet_ntop(their_addr.ss_family, GetInAddr((struct sockaddr *)&their_addr),
                          ip, sizeof(ip));
                printf("server: got a connection from %s\n", ip);

                res = recv(cfd_socket, data, MAX_LEN, 0);
                if (res == 0 || res == -1)
                {
                    fprintf(stderr, "server: recv'd no bytes (%s)\n", strerror(errno));
                    return 0;
                }

                struct httprequest *req = ParseHTTPRrequest(data);
                PrintHTTPRequest(req);
                struct httpresponse *response = GenerateHTTPResponse(req);
                SendHTTPResponse(response, cfd_socket);

                free(req);
                free(response);
                free(data);

                exit(0);
                break;
            }
            default: // parent
                // parent doesn't need client socket
                close(cfd_socket);
                break;
        }
        //sleep(1);//I found this helpful

    }
}